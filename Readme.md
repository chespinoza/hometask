# Coding Task

### Required .env file
```
export APP_SETTINGS=development
export FLASK_ENV=development
export FLASK_APP=run.py

```

### Run tests

`python manage.py test`

### Create docker container

`docker build -t coding-task:latest .`

### Run app using docker

`docker run --env-file .env-docker -p 8000:8000 coding-task` 