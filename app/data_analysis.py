import pandas as pd
import ujson

def get_sizes_aggregate(data, domain):
    """

    :param data: list of dicts containing flatten normalized records
    :param domain: string containing a domain, i.e: dailymotion.com
    :return: json string containing aggregate data for domain
    """
    df = pd.DataFrame(data)
    try:
        return df[(df.page_url.astype(str).str.contains(domain))]['creative_size'].value_counts().to_json()
    except Exception as e:
        print(e)
        return '[]'


def get_historic(data, referer_domain):
    """

    :param data: list of dicts containing flatten normalized records
    :param referer_domain: string containing headers_Referer field
    :return: json string containing records stored
    """
    try:
        return ujson.dumps([obj for obj in data if obj.get('headers_Referer') and referer_domain in obj['headers_Referer']])
    except Exception as e:
        print(e)
        return '[]'
