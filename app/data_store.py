import ujson


class Store(list):
    """Data structure used to store imported data from JSON data format
    Not a great Idea, just for demonstration purposes, the data should be stored
    in a persistent datastore that could manage concurrent access as cassandra, postgresql, etc.
    """

    def load_from_file(self, file_name):
        """Load JSON test data from a file as dicts

        :param file_name: file to load
        """

        try:
            with open(file_name) as data_file:

                for line in data_file:
                    self.load_json(line)

        except IOError as e:
            e.strerror = 'Unable to load file (%s)' % e.strerror
            raise

    def load_json(self, json_str):
        """Read a string(line on this context) and loaded it as a flattened dict

        :param json_str: string containing json data
        """

        data = ujson.loads(json_str)
        if not data.get('creative_size'):
            ad_width = data.get('ad_width')
            ad_height = data.get('ad_height')
            if ad_width and ad_height:
                data['creative_size'] = '{}x{}'.format(ad_width, ad_height)
        self.append(flatten_json(data))


def flatten_json(y):
    """Flat decoded obj from JSON data format"""

    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '_')
                i += 1
        else:
            out[name[:-1]] = x

    flatten(y)
    return out
