#
# In postgresql it could be used a schema like this
#
#   CREATE TABLE ingested_data (
#     id bigserial NOT NULL,
#     source varchar(255) NOT NULL,
#     data jsonb
#   )
#