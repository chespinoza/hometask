from flask import request, jsonify, make_response
from flask_api import FlaskAPI
import ujson
from instance.config import app_config
from app.data_store import Store
from app import data_analysis as da


# !!!!Global State, no shared state between workers!!!!
storage = Store()


def create_app(config_name):
    """Creates an Flask app into a env context(dev, test, etc)

    :param config_name: dev env context
    :return: returns FlaskAPI app instance
    """
    app = FlaskAPI(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')

    @app.route('/api/v1/collect', methods=['POST'])
    def collect_handler():
        """Receives json payloads and store them"""
        # import pdb;pdb.set_trace()

        data = request.get_data()

        try:
            storage.load_json(data)
        except Exception as e:
            app.logger.debug(e.with_traceback())
            return make_response(ujson.dumps({'result': 'error'})), 500

        return make_response(ujson.dumps({'result': 'ok'})), 201

    @app.route('/api/v1/history', methods=['GET'])
    def history_handler():
        """ """
        referer_domain = request.args.get('referer_domain')
        historic_json_str = da.get_historic(storage, referer_domain)
        return make_response(historic_json_str), 200

    @app.route('/api/v1/query', methods=['GET'])
    def query_handler():
        """ """
        domain = request.args.get('domain')
        aggregate_json_str = da.get_sizes_aggregate(storage, domain)
        return make_response(aggregate_json_str), 200

    return app
