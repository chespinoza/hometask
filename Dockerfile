FROM python:3.6-alpine AS base

FROM base AS builder
WORKDIR /build

COPY requirements.txt /build

RUN apk add --no-cache gcc musl-dev zlib-dev zlib-dev libffi-dev openssl-dev ca-certificates alpine-sdk && \
    ln -s /usr/include/locale.h /usr/include/xlocale.h
RUN pip install --install-option="--prefix=/build" -r /build/requirements.txt

FROM base
COPY --from=builder /build /usr/local
COPY . /app

WORKDIR /app
CMD ["gunicorn","-w","4","--bind","0.0.0.0:8000","--worker-class","eventlet","--pythonpath","/app","run"]