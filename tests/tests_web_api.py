import unittest
import sys
import os
import ujson

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from app import create_app


TEST_DATA_FILENAME = 'tests/data/impressions-shortlist'
TEST_JSON_PAYLOAD = '{"ad_tag_id": "1479", "ad_id": "3108", "ad_width": "300", "source": "rubicon", "strategy_id": "1285", "flight_id": "1300", "campaign_id": "449", "ad_height": "250", "impression_id": "nCxZX", "headers": {"Content-Length": "", "Accept-Language": "fr-fr", "Accept-Encoding": "gzip, deflate", "Connection": "keep-alive", "Accept": "*/*", "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/600.6.3 (KHTML, like Gecko) Version/7.1.6 Safari/537.85.15", "Host": "t.mediagamma.com", "Content-Type": ""}, "exchange_id": "1", "page_domain": "dailymotion.com", "cookie": "7e3761de6483f73d50e50eefcad40b8a", "date": "2015-06-08 09:19:59", "user_id": "7e3761de6483f73d50e50eefcad40b8a", "page_url": "http://www.dailymotion.com/video/x17sexp_artistes-production-une-femme-pas-comme-les-autres_fun"}'


class TestWebAPI(unittest.TestCase):
    def setUp(self):
        self.app = create_app(config_name="testing")
        self.client = self.app.test_client
        # Load entire test data file
        with open(TEST_DATA_FILENAME) as f:
            for line in f:
                try:
                    self.client().post('/api/v1/collect', data=line)
                except Exception as e:
                    print(e)
                    sys.exit(1)

    def test_collect_handler(self):
        """ """
        res = self.client().post('/api/v1/collect', data=TEST_JSON_PAYLOAD)
        self.assertEqual(res.status_code, 201)
        self.assertIn(ujson.dumps({'result': 'ok'}), str(res.data))

    def test_history_handler(self):
        """ """
        res = self.client().get('/api/v1/history?referer_domain=x.cl')
        self.assertEqual(res.status_code, 200)
        self.assertIn(ujson.dumps([]), str(res.data))

    def test_query_endpoint(self):
        """ """
        res = self.client().get('/api/v1/query?domain=x.com')
        self.assertEqual(res.status_code, 200)
        expected_obj = {"728x90": 3, "300x250": 3}
        received_obj = ujson.loads(res.data)
        self.assertEquals(len(expected_obj), len(received_obj))
        self.assertDictEqual(expected_obj,received_obj)
        self.assertEqual(expected_obj['728x90'], received_obj['728x90'])


if __name__ == '__main__':
    unittest.main()
