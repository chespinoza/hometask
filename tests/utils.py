import sys
import requests


def stream_data(file, url, lines=0):
    """ Read a file and send the data line by line to the specified URL"""
    with open(file) as f:
        iteration = 0
        for line in f:
            try:
                r = requests.post(url=url, data=line, json=True)
                print(r.content)
                iteration += 1
                if iteration == lines:
                    break
            except requests.exceptions.RequestException as e:
                print(e)
                sys.exit(1)

