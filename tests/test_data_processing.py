import sys
import os
import unittest
import ujson

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from app import data_analysis as da
from app.data_store import Store

DATA_FILE_PATH = 'tests/data/impressions-shortlist'


class DataProcessingTestCase(unittest.TestCase):
    """Test case for specific functions to analyse data"""
    def setUp(self):
        self.data = Store()
        self.data.load_from_file(DATA_FILE_PATH)

    def test_get_sizes_aggregate(self):

        results = da.get_sizes_aggregate(self.data, 'black-feelings.com')
        self.assertEqual(results, '{"300x250":1}')
        results = da.get_sizes_aggregate(self.data, 'http://ros-anymedia.co.uk')
        self.assertEqual(results, '{"300x250":9}')
        results = da.get_sizes_aggregate(self.data, 'dominio.com')
        self.assertEqual(results, '{}')
        results = da.get_sizes_aggregate(self.data, '.io')
        self.assertEqual(results, '{"300x250":170,"728x90":149,"160x600":15,"120x600":2}')

    def test_get_historic(self):
        results = da.get_historic(self.data, 'dominio.com')
        self.assertEqual(results, '[]')

        results = da.get_historic(self.data, 'lamontagne.fr')
        expected_json = '[{"ad_tag_id":"1474","ad_id":"3112","ad_width":"300","source":"rubicon","strategy_id":"1285","flight_id":"1300","campaign_id":"449","ad_height":"250","impression_id":"KGNar","headers_Content-Length":"","headers_Accept-Language":"fr-FR,fr;q=0.8,en-US;q=0.6,en;q=0.4","headers_Accept-Encoding":"gzip,deflate,sdch","headers_Connection":"keep-alive","headers_Accept":"image\\/webp,*\\/*;q=0.8","headers_User-Agent":"Mozilla\\/5.0 (Windows NT 6.1) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/36.0.1985.143 Safari\\/537.36","headers_Host":"t.mediagamma.com","headers_Referer":"http:\\/\\/www.lamontagne.fr\\/auvergne\\/actualite\\/2015\\/06\\/08\\/avoir-20-ans-a-moulins-tour-dhorizon-des-lieux-ou-sortent-les-jeunes_11471289.html","headers_Cache-Control":"max-age=0","headers_Content-Type":"","exchange_id":"1","page_domain":"01-edito-atf-laplacemedia-5.fr","cookie":"62abb5bb6b3b1555364798d7f5040e0b","date":"2015-06-08 09:20:00","user_id":"62abb5bb6b3b1555364798d7f5040e0b","page_url":"http:\\/\\/01-edito-atf-laplacemedia-5.fr","creative_size":"300x250"},{"ad_tag_id":"1483","ad_id":"3107","ad_width":"728","source":"rubicon","strategy_id":"1285","flight_id":"1300","campaign_id":"449","ad_height":"90","impression_id":"SwZeM","headers_Content-Length":"","headers_Accept-Language":"fr-FR,fr;q=0.8,en-US;q=0.6,en;q=0.4,en-GB;q=0.2","headers_Accept-Encoding":"gzip, deflate, sdch","headers_Connection":"keep-alive","headers_Accept":"image\\/webp,*\\/*;q=0.8","headers_User-Agent":"Mozilla\\/5.0 (Windows NT 6.3; WOW64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/43.0.2357.81 Safari\\/537.36","headers_Host":"t.mediagamma.com","headers_Referer":"http:\\/\\/www.lamontagne.fr\\/auvergne\\/actualite\\/departement\\/cantal\\/2014\\/09\\/30\\/depuis-cinq-generations-la-famille-piganiol-perpetue-laventure-artisanale-du-parapluie-a-aurillac_11162660.html","headers_Cache-Control":"max-age=0","headers_Content-Type":"","exchange_id":"1","page_domain":"01-edito-atf-laplacemedia-5.fr","cookie":"1b77fd3fbbbe8501084c36a3462ede78","date":"2015-06-08 09:20:01","user_id":"1b77fd3fbbbe8501084c36a3462ede78","page_url":"http:\\/\\/01-edito-atf-laplacemedia-5.fr","creative_size":"728x90"}]'
        self.assertEqual(results, expected_json)
        objs = ujson.loads(expected_json)
        self.assertEqual(len(objs), 2)

        results = da.get_historic(self.data, 'primonumero')
        expected_json = '[{"ad_tag_id":"1470","ad_id":"3094","ad_width":"300","source":"rubicon","strategy_id":"1284","flight_id":"1299","campaign_id":"449","ad_height":"250","impression_id":"CHWli","headers_Content-Length":"","headers_Accept-Language":"it-IT,it;q=0.8,en-US;q=0.5,en;q=0.3","headers_Accept-Encoding":"gzip, deflate","headers_Connection":"keep-alive","headers_Accept":"image\\/png,image\\/*;q=0.8,*\\/*;q=0.5","headers_User-Agent":"Mozilla\\/5.0 (Windows NT 5.1; rv:38.0) Gecko\\/20100101 Firefox\\/38.0","headers_Host":"t.mediagamma.com","headers_Referer":"http:\\/\\/www.primonumero.it\\/index-re.php","headers_Content-Type":"","exchange_id":"1","page_domain":"primonumero.it","cookie":"667a49aa96bd74ef56bdc00b6f57e9c9","date":"2015-06-08 09:20:01","user_id":"667a49aa96bd74ef56bdc00b6f57e9c9","page_url":"http:\\/\\/www.primonumero.it\\/index-re.php","creative_size":"300x250"}]'
        self.assertEqual(results, expected_json)
        objs = ujson.loads(expected_json)
        self.assertEqual(len(objs), 1)
if __name__ == '__main__':
    unittest.main()
